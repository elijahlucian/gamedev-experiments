import { Engine } from "@babylonjs/core/Engines/engine";
import { Scene } from "@babylonjs/core/scene";
import { Vector3 } from "@babylonjs/core/Maths/math";
import { FreeCamera } from "@babylonjs/core/Cameras/freeCamera";
import { HemisphericLight } from "@babylonjs/core/Lights/hemisphericLight";
import { Mesh } from "@babylonjs/core/Meshes/mesh";

import { GridMaterial } from "@babylonjs/materials/grid";

// Required side effects to populate the Create methods on the mesh class. Without this, the bundle would be smaller but the createXXX methods from mesh would not be accessible.
import "@babylonjs/core/Meshes/meshBuilder";
import { MeshBuilder } from "babylonjs";

// Get the canvas element from the DOM.
const canvas = document.getElementById("renderCanvas");
const engine = new Engine(canvas);
var scene = new Scene(engine);
var camera = new FreeCamera("camera1", new Vector3(0, 5, -10), scene);
var time = 0;
var speed = 0.001;
var throttle = 2;

camera.setTarget(Vector3.Zero());
camera.attachControl(canvas, true);

// This creates a light, aiming 0,1,0 - to the sky (non-mesh)
var light = new HemisphericLight("light1", new Vector3(0, 1, 0), scene);

// Default intensity is 1. Let's dim the light a small amount
light.intensity = 1;

// Create a grid material
var material = new GridMaterial("grid", scene);
material.gridRatio = 0.1;

let boxCount = 12
let offset = boxCount / 2

var boxesv = []
var boxesh = []
for (let i = 0; i <= boxCount; i++) {
	let boxv = MeshBuilder.CreateBox('dankbox', {size: 1}, scene)
	boxv.i = i
	boxv.position.y = 2;
	boxv.position.x = i - offset;
	boxv.material = material
	boxesv.push(boxv)
	
	let boxh = MeshBuilder.CreateBox('dankor', {size: 1}, scene)
	boxh.i = i
	boxh.material = material
	boxh.position.x = i - offset;
	boxesh.push(boxh)
}

// Our built-in 'ground' shape. Params: name, width, depth, subdivs, scene
// var ground = Mesh.CreateGround("ground1", 6, 6, 2, scene);
// ground.material = material;

// Render every frame
engine.runRenderLoop(() => {
	let dt = scene.getEngine().getDeltaTime() * speed * throttle
	time += dt
	
	boxesv.forEach(box => {
		box.position.y = Math.sin(time + box.i) * 2
		box.rotation.y += dt * 0.001
	})
	
	boxesh.forEach(box => {
		box.position.y = Math.cos(time + box.i + 1) * 2
		box.position.z = Math.sin(time + box.i + 1) * 2
	})

	scene.render();
});