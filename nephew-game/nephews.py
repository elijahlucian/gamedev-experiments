import time, json

userInput = ''
lightState = 0
progression = 0
narrative = 0

linearEvents = {
    # { iterator : ['challenge' , ['solution','keywords'], 'reward phrase']
    0:['you are in a dark room.',['light','on'],'you are now able to look around...'],
    1:[]
    }

storyNarrative = [
    "You wake up...",
    "you hear a faint sound behind a wall",
    "and the hum of machines...",
    "next",
    "on the table lies a rare object, this object can take on any form",
    "to morph the runeStone, type 'runeStone = [whatever]' then press 'enter'",
    "try it now, look around and morph it into something useful",
    "next"
    ]

gameEnd = len(linearEvents) -1
roomDetails = []
roomOptions = ['']

def clearScreen():
    print("\n"*50)

def look():
    return 0

def gameNarrative(i):
    while storyNarrative[i] != "next":
        print(storyNarrative[i] + '\n')
        i = i + 1
        # input("\t"*10 + "press enter")
        input()
        # time.sleep(2)
    i = i + 1
    return i

def gameEvent(i):
    while True:
        print(linearEvents[i][0])
        userInput = input('what do you do? ')
        userInput.split(' ')
        if linearEvents[i][1][0] in userInput and linearEvents[i][1][1] in userInput:
            print(linearEvents[i][2])
            break
        else:
            print('that does not help \n')
            time.sleep(0.5)
            continue

def exploreRoom(i):
    print()
    return True

def unlockRoom(i):
    pass

while progression < gameEnd:
    clearScreen()
    narrative = gameNarrative(narrative)
    gameEvent(progression)
    exploreRoom(progression)
    unlockRoom(progression)
